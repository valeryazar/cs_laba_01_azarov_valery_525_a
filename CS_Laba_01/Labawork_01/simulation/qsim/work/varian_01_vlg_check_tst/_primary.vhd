library verilog;
use verilog.vl_types.all;
entity varian_01_vlg_check_tst is
    port(
        y1              : in     vl_logic;
        y2              : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end varian_01_vlg_check_tst;
