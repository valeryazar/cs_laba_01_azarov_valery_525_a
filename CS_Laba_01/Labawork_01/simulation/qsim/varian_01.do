onerror {quit -f}
vlib work
vlog -work work varian_01.vo
vlog -work work varian_01.vt
vsim -novopt -c -t 1ps -L cycloneii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.varian_01_vlg_vec_tst
vcd file -direction varian_01.msim.vcd
vcd add -internal varian_01_vlg_vec_tst/*
vcd add -internal varian_01_vlg_vec_tst/i1/*
add wave /*
run -all
